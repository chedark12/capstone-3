import React, {useState} from 'react';
import './App.css';
import NavBar from './components/NavBar';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from  'react-router-dom'; 

import Home from './pages/Home'
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import UserContext from './UserContext';
import AddProduct from './pages/AddProduct';
import ViewCart from './pages/ViewCart';
import UserDetails from './pages/UserDetails';

function App() {

const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'

    }); 
 
 const unsetUser = () =>{
  	localStorage.clear();
  	setUser({
      accessToken: null,
      isAdmin : null
      })
  }

  return (
   
    <UserContext.Provider value={{user, setUser, unsetUser}}>
      <Router>
        <NavBar/>
        
        <Container>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register} />
            <Route exact path="/allActiveProducts" component={Products}  />     
            <Route exact path="/addProduct" component={AddProduct}  />      
            <Route exact path="/viewCart" component={ViewCart}  />      
            <Route exact path="/details" component={UserDetails}  />     
            <Route component={NotFound}  />      
          </Switch>
        </Container>	
      </Router> 
    </UserContext.Provider>
  );
}

export default App;
