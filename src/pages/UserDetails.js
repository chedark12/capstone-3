import React, {useEffect, useState, useContext} from 'react';
import {Tab, Row, Col, Nav, ListGroup, Card, ListGroupItem} from 'react-bootstrap';
import UserContext from	'../UserContext';

export default function UserDetails(){
	
	const [userDetails, setUserDetails] = useState('');
	const [orderHistory, setOrderHistory] = useState('');
	const {user} = useContext(UserContext);
	let noData = false;
	useEffect(()=>{
		fetch('https://morning-falls-34881.herokuapp.com/users/details',{
			headers:{
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setUserDetails(data.map(detail=>{
				return(
					<>
			          <p>
			        	 <span>First Name: </span>
				         <label>{detail.firstName}</label>
		        	 </p>
		        	 <p>
			        	 <span>Last Name: </span>
				         <label>{detail.lastName}</label>
		        	 </p>
		        	 <p>
			        	 <span>Email: </span>
				         <label>{detail.email}</label>
		        	 </p>
		        	 <p>
			        	 <span>Mobile No.: </span>
				         <label>{detail.mobileNo}</label>
		        	 </p>
					</>
				)
			}))

			if (user.isAdmin === true) {
				fetch('https://morning-falls-34881.herokuapp.com/users/viewOrder',{
					headers:{
						'Content-Type' : 'application/json',
						'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
					}
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)

					setOrderHistory(data.reverse().map(orders=>{
						return(
							<>
								<Card className="m-3">
									<Card.Header as="h5">Order Reference #:  {orders._id} </Card.Header>
									<Card.Body>
									  <Card.Title className="text-muted">Ordered by user: {orders.userId}</Card.Title>									 							    
									  <Card.Title>{orders.orderItems[0].productName}</Card.Title>
									  <Card.Text>
									    {orders.orderItems[0].description}
									  </Card.Text>								    
									</Card.Body>
									<ListGroup className="list-group-flush">
									  <ListGroupItem>Order Status: <span className="text-success">{orders.orderStatus}</span></ListGroupItem>								    
									  <ListGroupItem>Total : {orders.total}</ListGroupItem>
									</ListGroup>
									<Card.Footer className="text-muted">Date Ordered: {orders.orderDate}</Card.Footer>
								</Card>
{/*
								<ListGroup className="mb-5">
									<ListGroup.Item>
										<div>
											<p>
									        	 <span>Order Reference: </span>
										         <label>{orders._id}</label>
								        	</p>
											<p>
									        	 <span>Order Date: </span>
										         <label>{orders.orderDate}</label>
								        	</p>
								        	<p>
									        	 <span>Order Status: </span>
										         <label>{orders.orderStatus}</label>
								        	</p>
								        	<p>
									        	 <span>Ordered by user: </span>
										         <label>{orders.userId}</label>
								        	</p>
								        	<p>									        	
										        <label>Ordered Items</label>
										        <ListGroup>
												  <ListGroup.Item>
											  		{orders.orderItems[0].productName}  
												  	<p>price: {orders.orderItems[0].price}</p>
												  </ListGroup.Item>
												</ListGroup>
								        	</p>
								        	<p>
									        	 <span>Total: </span>
										         <label>{orders.total}</label>
								        	</p>
										</div>
									</ListGroup.Item>

								</ListGroup>*/}
							</>

						)
					}))
				})
			}else{

				

					setOrderHistory(data.map(orders=>{
						if (orders.order=='') {
							return(
								<>
									<Card className="m-3">
										<Card.Header as="h5">Order History </Card.Header>
										<Card.Body>
										 
										  <Card.Text>
										    Order History is empty
										  </Card.Text>								    
										</Card.Body>
										
									</Card>
								</>
							)
						}else{
							return(

								orders.order.reverse().map(order=>{
									return(

										<>

										<Card className="m-3">
											<Card.Header as="h5">Order Reference #:  {order._id} </Card.Header>
											<Card.Body>
											  <Card.Title>{order.orderItems[0].productName}</Card.Title>
											  <Card.Text>
											    {order.orderItems[0].description}
											  </Card.Text>								    
											</Card.Body>
											<ListGroup className="list-group-flush">
											  <ListGroupItem>Order Status: <span className="text-success">{order.orderStatus}</span></ListGroupItem>								    
											  <ListGroupItem>Total : {order.total}</ListGroupItem>
											</ListGroup>
											<Card.Footer className="text-muted">Date Ordered: {order.orderDate}</Card.Footer>
										</Card>
										
										</>
									)
								})
							)
						}
					}))
				
	
			}
			

		})




	},[])
	

		return(
			
			<>
				<h2 className="text-center">My Profile</h2>
				<Tab.Container id="left-tabs-example" defaultActiveKey="first">
				  <Row>
				    <Col sm={3}>
				      <Nav variant="pills" className="flex-column">
				        <Nav.Item>
				          <Nav.Link eventKey="first">Profile Information</Nav.Link>
				        </Nav.Item>
				        <Nav.Item>
				          <Nav.Link eventKey="second">Order History</Nav.Link>
				        </Nav.Item>
				      </Nav>
				    </Col>
				    <Col sm={9}>
				      <Tab.Content>
				        <Tab.Pane eventKey="first">
			        		{userDetails}
				        </Tab.Pane>

				        <Tab.Pane eventKey="second">

				         	{orderHistory}
				        </Tab.Pane>
				      </Tab.Content>
				    </Col>
				  </Row>
				</Tab.Container>
			</>
		)
}