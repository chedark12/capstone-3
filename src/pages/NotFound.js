import React from 'react';
import {Link} from	'react-router-dom';

export default function NotFound(){
	return(
		<div id="NotFound">
            <h3>This page could not be found/Error 404</h3>
            <h4>Go back to <Link as={Link} to="/">Home</Link></h4> 

        </div>
	
	)
}