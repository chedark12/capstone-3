
import React, {useContext, useEffect, useState} from 'react';
//import courseData from '../data/courses'; // to acquire the actual data that we want to display, describes all the courses records from the data folder
import UserContext from	'../UserContext';
import {Table, Card, CardGroup} from 'react-bootstrap';
import DeleteButton from '../components/DeleteButton';
import UpdateButton from '../components/UpdateButton';
import AddToCartButton from '../components/AddToCartButton'; 



export default function Products(){
	
	const [adminProducts, setAdminProducts] = useState([]);
	
	const {user} = useContext(UserContext);
	console.log(user);

	useEffect(() => {
		
		fetch('https://morning-falls-34881.herokuapp.com/products/allProducts', {


		})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if (user.isAdmin === true) {


					setAdminProducts(data.map(product => {
						return(
							
								<tr>
									<td>{product._id}</td>
									<td>{product.name}</td>
									<td>{product.description}</td>
									<td>{product.price}</td>
									<td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Active" : "Inactive"}</td>
									<td><DeleteButton productId={product._id}/></td>
									<td><UpdateButton productId={product._id}/></td>
								</tr>
							

							)

					}))
				}else{
					
					fetch('https://morning-falls-34881.herokuapp.com/products/allActiveProducts',{

					})
					.then(res => res.json())
					.then(data => {
						console.log(data);
						setAdminProducts(data.map(product => {
							
							return(									

								<Card className="m-3 border border-black  ">
								    <Card.Body className="">
								    	<Card.Img variant="top" src="https://image-placeholder.com/images/actual-size/57x57.png" />
								    	<Card.Title className="p-1">{product.name}</Card.Title>
								    	<Card.Text>
								    	  {product.description}
								    	</Card.Text>
								    	<td className="justify-content-center"><AddToCartButton productId={product._id}/></td>
								    </Card.Body>
							  	</Card>
							 	
							)

						}))
					})
						
				}

			})
		


	},[])


	
		if (user.isAdmin === true) {
			return (
				
				<>
					<h1 className="text-center mb-5">Products Dashboard</h1>
					<Table striped bordered hover>
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Description</th>
								<th>Price</th>
								<th>Status</th>
							</tr>
						</thead>

						<tbody>
							{adminProducts}
						</tbody>
					</Table>
				</>
			)

		}else{

		
			return(

				<>
					<h2 className="text-center">Products</h2>
					<CardGroup>
					{adminProducts}
					</CardGroup>
						
				</>

			)

		}

		

	


}