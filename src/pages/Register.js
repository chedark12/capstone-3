import React, {useState, useEffect, useContext}from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from	'../UserContext';
import {Redirect, useHistory} from 'react-router-dom';

export default function Register(){
	const history = useHistory();
	//  let's define state hooks for all input fields
	const {user, setUser} = useContext(UserContext)
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');

	// let's declare a variable that will describe the state of the register button component
	const [registerButton, setRegisterButton] = useState(false);
	// out next task, let us try to refactor our register user function

	useEffect( () => {
			if ((firstName !== '' && lastName !== '' && mobileNo !== '' && email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
				setRegisterButton(true);
			
			}else{

			// here in the else, let's describe the retirn if any of the condition has not met
			setRegisterButton(false);
			
			}
		}, [firstName, lastName, mobileNo, email, password, verifyPassword])
// EXPLANATION FOR THE SCRIPT
// the values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is called two-way binding

// What is two way binding?
//  Two data binding means the data we changed in the view(UI) has updated the state.
// The data in the state has updated the view

// let's create a function that will simulate an actual register page
// we want to add an alert if registration is successful

	function registerUser(e){

		e.preventDefault(); // this is to avoid page redirection
		

		fetch('https://morning-falls-34881.herokuapp.com/users/register',{
			method : 'POST',
		 	headers: {'Content-Type' : 'application/json'},
		 	body: JSON.stringify({
		 		firstName : firstName,
		 		lastName : lastName,
		 		mobileNo : mobileNo,
		 		email : email,
		 		password : password
			})
		 })
		 .then(res => res.json())
		 .then(data => {
		 		console.log(data);

		 		Swal.fire({
					 	title: "Yaaaaaaaaaay!!",
						icon: "success",
						text: "Successfully Registered"
				})

		 			history.push('/login');


				



		 	})

	} 
	
	if (user.accessToken !== null) {
		return <Redirect to="/" />
		// if the user.email has a value, instead of showing the login component, go tothe route indicated in the "to" prop of Redirect component
	}

	return(
		<Container className="px-5" >
		<Form onSubmit={(e)=> registerUser(e)} >

			<Form.Group controlId="userFName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value = {firstName} onChange={e => setFirstName(e.target.value)} required/>
			</Form.Group>

			<Form.Group controlId="userLName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value = {lastName} onChange={e => setLastName(e.target.value)} required/>
			</Form.Group>

			<Form.Group controlId="userMobileNum">
				<Form.Label>Mobile No:</Form.Label>
				<Form.Control type="text" placeholder="Enter Mobile Number" value = {mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
			</Form.Group>		

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control type="email" placeholder="Enter email" value = {email} onChange={e => setEmail(e.target.value)} required/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter password" value = {password} onChange={e => setPassword(e.target.value)}  required />
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control type="password" placeholder="Verify password" value = {verifyPassword} onChange={e => setVerifyPassword(e.target.value)}  required />
			</Form.Group>
			{registerButton ? <Button variant="primary" type="submit">Submit</Button> 
			: 
				<Button variant="primary" type="submit" disabled>Submit</Button> 
			}
			
			
		</Form>
		</Container>
	)
}