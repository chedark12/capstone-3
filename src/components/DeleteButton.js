import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useHistory} from	'react-router-dom';


export default function DeleteButton({productId}){
	const history = useHistory();
	function deleteProduct(e){
		e.preventDefault();

		fetch(`https://morning-falls-34881.herokuapp.com/products/${productId}/deleteProduct`,{
			method : 'DELETE',
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {
				Swal.fire({
					title	: 'Successful',
					icon	: 'success',
					text	: 'The product is deleted'
				})

				
				.then(()=>history.go(0))//window.location.reload()
			}else{
				Swal.fire({
					title	: 'Error',
					icon	: 'error',
					text	: 'Failed to delete a product'
				})
			}

		})	
	}//end of function

	return(

		<Button variant="danger" onClick={deleteProduct}>Delete</Button>

	)
	
}