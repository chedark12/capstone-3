import React,{Fragment, useContext} from 'react';
// import necessary components
import Navbar from 'react-bootstrap/Navbar'; // export default = can be changed or placed on any variable
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink, useHistory} from	'react-router-dom';
import UserContext from '../UserContext';


	
export default function	NavBar(){

	const history = useHistory(); // the useHistory hook gives access to the history instance that you may use to navigate or to access a specific location
	// push(path) pushes a new entry onto the history stack

	// usercontext() is a react hook used to unwrap our contect. It will return the data passed as value by a provider
	const {user, setUser, unsetUser} = useContext(UserContext);
	// console.log(user)

	const logout = () => {
		unsetUser();
		setUser({accessToken:null});
		history.push('/login');
	}

	let rightNav = (user.accessToken !== null) ? 
		user.isAdmin
		?
		<Fragment>
			<Nav.Link as={NavLink} to="/addProduct" >Add Product</Nav.Link>
			<Nav.Link as={NavLink} to="/details" >Dashboard</Nav.Link>
			<Nav.Link onClick={logout}>Logout</Nav.Link>
		</Fragment>

		:
		
		<Fragment>
			<Nav.Link as={NavLink} to="/viewCart">View Cart</Nav.Link>
			<Nav.Link as={NavLink} to="/details">Profile</Nav.Link>				
			<Nav.Link onClick={logout}>Logout</Nav.Link>	
		</Fragment>
		:
		<Fragment>
			<Nav.Link as={NavLink} to="/login">Login</Nav.Link>				
			<Nav.Link as={NavLink} to="/register">Register</Nav.Link>	
		</Fragment>


	return(
		<Navbar bg="dark" variant="dark" expand="lg">
			<Navbar.Brand as={Link} to="/">Supplies</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/allActiveProducts">Products</Nav.Link>							
				</Nav>
				<Nav className="ml-auto">
					{rightNav}
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}

