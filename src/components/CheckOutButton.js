import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useHistory} from	'react-router-dom';

export default function CheckOutButton({orderId}){
	const history = useHistory();
	function checkOut(e){
		e.preventDefault();

		fetch('https://morning-falls-34881.herokuapp.com/users/checkOut',{
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data=>{
				console.log(data)

			if (data === true) {
				Swal.fire({
					icon	: 'success',
					text	: 'Thank you for shopping'
				})

				
				.then(()=>history.push('/'))//window.location.reload()
			}else{
				Swal.fire({
					title	: 'Error',
					icon	: 'error',
					text	: 'Failed to check out'
				})
			}

		})



	}

	return(

		<Button variant="primary" onClick={checkOut}>Check Out</Button>

	)


}