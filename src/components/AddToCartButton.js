import React, {useState, useEffect, useContext} from 'react';
import UserContext from	'../UserContext';
import {Button, Nav} from 'react-bootstrap';
import {NavLink, Link} from 'react-router-dom'
import Swal from 'sweetalert2';

export default function AddToCartButton({productId}){
	const [isActive, setIsActive] = useState(false);
	const {user} = useContext(UserContext);
	useEffect(()=>{

		if (user.accessToken === null) {
			setIsActive(false)
		}else{
			setIsActive(true)
		} //end of if

	},[])

	function addToCart(e){
		e.preventDefault();
		fetch('https://morning-falls-34881.herokuapp.com/users/addToCart',{
			method	: 'POST',
			headers	: {
				'Content-Type'	: 'application/json',
				'Authorization'	: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
		 		productId : productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				Swal.fire({
					title : 'Success',
					icon : 'success',
					text : 'Item added to Cart Successfully'
				})
			}else{
				Swal.fire({
					title : 'Error',
					icon : 'error',
					text : 'Something went wrong!'
				})
			}
		})
		
		}


	return(

		
    		isActive ?
    		
			<Button variant="primary" onClick={addToCart}>Add to cart</Button>
			:
			<Button><Link as={NavLink} to="/login" className="text-white">Login required</Link></Button>
		
		// <Button  variant="primary" onClick={addToCart}>Add to cart</Button>
	)
		


}