import React from 'react';
import {Jumbotron, Button, Row, Col, Nav} from 'react-bootstrap';
import {NavLink} from 'react-router-dom'
import '../App.css';

export default function Banner(){
	return(
		<Row className="mt-2">
			<Col >
				<Jumbotron className="jumboBg">
					<h1 className="text-white">Buy Supplies Now!</h1>
					<p className="text-white">The most affordable place to buy supplies</p>
					<Button><Nav.Link as={NavLink} to="/allActiveProducts"   className="text-white">View All Products</Nav.Link></Button>
				</Jumbotron>
			</Col>	
		</Row>
	)

}