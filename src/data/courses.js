// this will be used as the records about the courses that the users will be able to book.
// or it is our mock data of courses
//  we now have to create an array of objects which will hold the properties and information of our data files which will later be exported across our application


export default [
	{ 
		id: "wdc001", // the value which will be given in the id property will serve as a reference number to keep track of the available courses
		name: "PHP - Laravel", 						
		description: " Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Beatae illum repellat earum veniam ratione cupiditate totam, repellendus tenetur ab fuga vel? Nisi magnam hic fuga illo et, natus ducimus quisquam.",
		price: 45000,
		onOffer: true // will determine if a course is being offered by the org or by the school

	},

	{ 
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor, sit amet, consectetur adipisicing elit. Suscipit nostrum saepe, ducimus commodi quisquam, atque eum? Nesciunt recusandae ipsum expedita quos, quod nihil soluta laudantium placeat illum, cumque, voluptas. Aut.",
		price: 50000,
		onOffer: true

	},

	{ 
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Exercitationem eveniet a dolore minima officia ratione doloremque non, neque quas numquam quasi et accusamus magnam incidunt ab nihil aperiam similique quidem?",
		price: 55000,
		onOffer: true

	},

	{ 
		id: "wdc004",
		name: "234234",
		description: "3243",
		price: 1,
		onOffer: true

	}

]